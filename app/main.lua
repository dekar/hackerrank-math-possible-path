﻿local suit = require "suit"

local lg = love.graphics

local aSlider = {value = 10, min = -100, max = 100, step = 10}
local bSlider = {value = 10, min = -100, max = 100, step = 10}

function love.load(arg)
    canvas = lg.newCanvas(lg.getDimensions())
    assert(canvas)

    renderCanvas()
end

function renderCanvas()
    local w, h = lg.getDimensions()
    local xc = w / 2
    local yc = h / 2
    local a = aSlider.value
    local b = bSlider.value

    function point(a, b)
        lg.circle("fill", xc + a, yc + b, 2)
    end

    function placePoint(a, b, stepNum)
        lg.setColor({255, 255, 255, 255})
        point(a, b)
        lg.setColor({200, 200 / stepNum, stepNum * 20, 255})
        point(a + b, b)
        point(a, a + b)
        point(a - b, b)
        point(a, a - b)
        if stepNum >= 0 then
            placePoint(a, a + b, stepNum - 1)
            placePoint(a + b, b, stepNum - 1)
            placePoint(a - b, b, stepNum - 1)
            placePoint(a, a - b, stepNum - 1)
        end
    end

    lg.setCanvas(canvas)
    lg.clear()

    lg.setColor({200, 200, 100, 255})
    lg.line(0, yc, w, yc)
    lg.line(w / 2, 0, w / 2, h)

    placePoint(a, b, 5)
    lg.setColor({200, 200, 100, 255})

    lg.setCanvas()
end

function love.update(dt)
    local _, h = lg.getDimensions()

    suit.layout:reset(0)
    suit.layout:padding(3, 3)

    local sliderW = 200
    local sliderH = 20

    if suit.Slider(aSlider, 0, 0, sliderW, sliderH).changed then
        renderCanvas()
    end
    suit.Label(tostring(aSlider.value), {align = "left"}, sliderW, 0, 100, sliderH)
    
    if suit.Slider(bSlider, 0, sliderH, sliderW, sliderH) then
        renderCanvas()
    end
    suit.Label(tostring(bSlider.value), {align = "left"}, sliderW, sliderH, 100, sliderH)
end

function love.draw()
    local w, h = lg.getDimensions()
    lg.draw(canvas)
    suit.draw()
end

function love.keypressed(key)
    if key == "escape" then
        love.event.quit()
    end
end

function love.quit()
end

